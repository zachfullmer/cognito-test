import Vue from "vue";
import App from "./App.vue";
import SignIn from "./SignIn.vue";

new Vue({
  el: "#app",
  render: h => h(SignIn)
});
